CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "HOST": "launchpad-postgres",
        "USERNAME": "postgres",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "kratos-alpha2",
        "PORT": "5432",
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "alpha2",
        "DSN": "https://f35ea954bf264aa9aaa1014de9cdf226@sentry.livspace.com/78"
    },
    "CACHE": {
        "NAME": "REDIS",
        "ENABLED": False,
        "REDIS": {
            "HOST": "redis-kratos",
            "PORT": 26379,
            "SOCKET_TIMEOUT": 0.1,
            "PASSWORD": "",
            "PREFIX": "kratos-temp-alpha2",
            "USE_SENTINEL": True,
            "SENTINEL_SERVICE_NAME": "redis-kratos"
        },
        "TIMEOUT": {
            "BOUNCER": 86400
        }
    }
}

GATEWAY = {
    "HOST": "axle",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    "CIVITAS": {
        "HOST": "civitas.alpha2.livspace.com",
        "HEADERS": {            
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/civitas'
        },
    },
    "LAUNCHPAD": {
        "HOST": "launchpad-backend.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/launchpad'
        }
    },
    "BOUNCER": {
        "HOST": "bouncer.alpha2.livspace.com",
        "HEADERS": {
            "Content-Type": "application/json",
            "X-CLIENT-ID": "LAUNCHPAD",
            "X-CLIENT-SECRET": "6ee38134-64c0-4095-af8a-38f05e69b0bd"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/bouncer'
        }
    }
}


CONFIG = {
    "DJANGO_LOG_LEVEL": "INFO",
    "DATABASE": {
        "URI_SCHEME": "mongodb+srv",
        "HOST": "canvas-mongo-test.akmgb.mongodb.net",
        "USERNAME": "livspace",
        "PASSWORD": "livspaceadmin",
        "DB_NAME": "enigma-beta",
    },
    "EVENT": {
        "ENV": "local"
    },
    "DEBUG": False,
    "ALLOWED_HOSTS": ["*"],
    "ENV": "local",
    "SENTRY": {
        "ENABLED": True,
        "ENVIRONMENT": "alpha2",
        "DSN": "https://f0afcad5fe8344188150ffc97cb2befe@sentry.livspace.com/91"
    },
    "MASTER_TOKEN": "zwOh6fJQN5vZTnH9CyjyJETBAWRBaFzQ"
}


GATEWAY = {
    "HOST": "api.alpha2.livspace.com",
    "HEADERS": {
        "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
        "Content-Type": "application/json"
    }
}

EXTERNAL_SERVICES = {
    'BOUNCER': {
        'HOST': 'api.alpha2.livspace.com/bouncer',
        'HEADERS': {
            "Authorization": "Basic U3Rhck1TRmUtRzREc0tJOjJTWGhMeEc3cHJhYmtXbDJySFFwdGJwSXl0OFhIOXln",
            "X-Requested-By": "0",
            "Content-Type": "application/json"
        },
        "GATEWAY": {
            "ENABLED": True,
            "PATH": '/bouncer'
        }
    }
}

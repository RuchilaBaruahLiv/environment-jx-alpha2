#!/bin/bash

newrelic-admin run-program uwsgi docket/conf/uwsgi.ini --http 0.0.0.0:8000
celery -A docket beat -l INFO --detach
celery -A docket worker -l INFO --concurrency=2 --max-tasks-per-child=2

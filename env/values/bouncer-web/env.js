const oidcSettings = {
  "authority": "https://auth.alpha2.livspace.com",
  "clientId": "whitstable",
  "redirectUri": "https://bouncer.alpha2.livspace.com/admin/callback",
  "responseType": "token id_token",
  "scope": "openid offline",
  "loadUserInfo": true,
  "automaticSilentRenew": true,
  "silent_redirect_uri": "https://bouncer.alpha2.livspace.com/admin/callback-silent"
}
const whitstableConf = {
  gw_url: 'https://api.alpha2.livspace.com',
  bouncer_url: 'https://bouncer.alpha2.livspace.com'
}
